using Gee;

public interface Ros.Helpable : GLib.Object {
  public abstract Ros.HelpSequence get_help_sequence();
}

public class Ros.HelpStep {
  public Gtk.Widget       widget;
  public string           message;
  public Gtk.PositionType position;

  public HelpStep(Gtk.Widget w, string m, Gtk.PositionType p = Gtk.PositionType.LEFT) {
    message = m;
    widget = w;
    position = p;
  }
}

public class Ros.HelpSequence {
  private Array<Ros.HelpStep>           sequence;
  private HashMap<string, Ros.HelpStep> steps;

  public HelpSequence() {
    sequence = new Array<Ros.HelpStep> ();
    steps = new HashMap<string, Ros.HelpStep> ();
  }

  public void register_step(string n, Ros.HelpStep s) {

    steps.set(n, s);
  }

  public Array<Ros.HelpStep> get_sequence() {
    return sequence;
  }

  public void order_sequence(string[] order) {
    if (sequence.length > 0) {
      sequence.remove_range(0, sequence.length - 1);
    }

    for (int i = 0; i < order.length; i++) {
      if (steps.has_key(order[i])) {
        var val = steps[order[i]];
        sequence.append_val(val);
      } else {
        stderr.printf("order_sequence - key '%s' not found. Skipping.\n", order[i]);
      }
    }
  }
}

public class Ros.HelpPopover : Gtk.Popover {
  public Array<Ros.HelpStep>  sequence;
  public uint                 n_steps;
  public uint                 current_step = 0;

  public Gtk.Label            message;
  public Gtk.Label            step_indicator;
  public bool                 initial_sensitivity;

  public HelpPopover(Array<Ros.HelpStep> s) {
    sequence = s;
    n_steps = sequence.length;
    style_popover();

    var vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
    this.add(vbox);

    message = new Gtk.Label("");
    message.margin = 5;
    message.max_width_chars = 25;
    message.set_line_wrap(true);
    message.valign = Gtk.Align.CENTER;
    vbox.pack_start(message, true, true, 0);

    var hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
    hbox.margin = 10;
    vbox.pack_start(hbox, false, false, 0);

    var prev_button = new Gtk.Button.with_label("←");
    prev_button.halign = Gtk.Align.START;
    prev_button.clicked.connect(() => {
      switch_to_step(prev_step());
    });
    hbox.pack_start(prev_button, false, false, 0);

    step_indicator = new Gtk.Label("");
    step_indicator.halign = Gtk.Align.FILL;
    hbox.pack_start(step_indicator, true, true, 0);

    var next_button = new Gtk.Button.with_label("→");
    next_button.halign = Gtk.Align.END;
    next_button.clicked.connect(() => {
      switch_to_step(next_step());
    });
    hbox.pack_start(next_button, false, false, 0);

    this.closed.connect(() => {
      // restore current widget
      sequence.index(current_step).widget.sensitive = initial_sensitivity;
      lowlight_widget(sequence.index(current_step).widget);
    });

    point_to(current_step);
    this.show_all();
    this.popup();
  }

  public uint next_step() {
    return (current_step + 1 == n_steps) ? 0 : current_step + 1;
  }

  public uint prev_step() {
    return (current_step == 0) ? n_steps - 1 : current_step - 1;
  }

  public void point_to(uint step) {
    highlight_widget(sequence.index(step).widget);

    // Make next widget sensitive
    initial_sensitivity = sequence.index(step).widget.sensitive; // backup
    sequence.index(step).widget.sensitive = true; // Set

    // point to next widget
    set_relative_to(sequence.index(step).widget);
    position = sequence.index(step).position;

    // update message
    message.set_markup(sequence.index(step).message);

    // Update step # indicator
    var i = step + 1;
    step_indicator.label = i.to_string() + " / " + n_steps.to_string();
  }

  public void switch_to_step(uint step_number) {
    uint  p_step = current_step; // backup step # of the widget we are about to leave (the "previous" vwidget)
    bool  p_sensitivity = initial_sensitivity; // backup sensitivity of the widget we are about to leave

    lowlight_widget(sequence.index(p_step).widget);

    // Point to desired step
    point_to(step_number);

    // restore sensitive of previous widget
    sequence.index(p_step).widget.sensitive = p_sensitivity;

    current_step = step_number;
  }

  public void style_popover() {
    width_request = 300;
    height_request = 200;
    Ros.StyleHelper.add_class(this, "help_popover");
  }

  public void highlight_widget(Gtk.Widget w) {
    Ros.StyleHelper.add_class(w, "help_highlighted_widget");
  }

  public void lowlight_widget(Gtk.Widget w) {
    Ros.StyleHelper.remove_class(w, "help_highlighted_widget");
  }
}
