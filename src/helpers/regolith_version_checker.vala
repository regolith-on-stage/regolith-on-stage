public class Ros.RegolithVersionChecker {

  public static bool check_compatibility(string min_version) {
    string  output = "", error = "";
    int     exit_status;

    Process.spawn_command_line_sync("cat /etc/regolith/version", out output, out error, out exit_status);
    if (exit_status != 0) {
      stdout.printf("Unable to get Regolith version: %s\n", error);
      return false;
    }

    Regex     re = new Regex("REGOLITH_VERSION=(.*)");
    string[]  sp = re.split(output);
    string    reg_version = sp[1];

    stdout.printf("REG VERSION: [%s]\n", reg_version);

    if (reg_version < min_version) {
      return false;
    }

    return true;
  }

  public static Gtk.Box get_wrong_version_widget (string min_version) {
    var box = new Gtk.Box(Gtk.Orientation.VERTICAL, 20);

    var img = new Gtk.Image.from_resource("/other/wrong_version.svg");
    box.pack_start(img, false, false, 0);

    var lbl_msg = new Gtk.Label("");
    lbl_msg.set_markup("<span size=\"large\">This feature is only available starting from Regolith v" + min_version + ".</span>");
    box.pack_start(lbl_msg, false, false, 0);

    var lbl_upgrade = new Gtk.Label("");
    lbl_upgrade.set_markup("<a href=\"https://regolith-linux.org\">Upgrade to the lastest Regolith version now!</a>");
    box.pack_start(lbl_upgrade, false, false, 0);

    return box;
  }
}
