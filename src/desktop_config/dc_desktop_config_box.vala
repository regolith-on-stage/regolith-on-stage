using Gee;

public class Ros.DesktopConfigBox : Gtk.Box, Ros.Helpable {
  public HashMap<string, string>            macros;
  public HashMap<string, Ros.VariableValue> variables;
  public Ros.ParserManager                  parser_manager;

  public Ros.VariablesTree                  variables_tree;
  public Ros.VariablesEditor                variables_editor;
  public Gtk.SearchEntry                    filter_entry;

  public Ros.HelpSequence                   help = null;

  private string                            min_version = "1.3.1";

  public DesktopConfigBox () {
    orientation = Gtk.Orientation.HORIZONTAL;
    spacing = 10;

    if (Ros.RegolithVersionChecker.check_compatibility(min_version)) {
      help = new Ros.HelpSequence();
      parse_config();

      // Left side
      var variables_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
      this.pack_start(variables_box, true, true, 0);

      var variables_box_label = new Gtk.Label(null);
      Ros.StyleHelper.add_class(variables_box_label, "desktop_config_header");
      variables_box_label.label = "List of Variables";
      variables_box_label.halign = Gtk.Align.START;
      variables_box_label.margin_bottom = 10;
      variables_box.pack_start(variables_box_label, false, false, 0);

      filter_entry = new Gtk.SearchEntry();
      filter_entry.halign = Gtk.Align.CENTER;
      filter_entry.width_request = 300;
      filter_entry.hexpand = true;
      filter_entry.changed.connect(() => {
        variables_tree.filter_model.refilter();
      });
      variables_box.pack_start(filter_entry, false, false, 0);

      var scrolled_window = new Gtk.ScrolledWindow(null, null);
      variables_box.pack_start(scrolled_window, true, true, 0);

      variables_tree = new Ros.VariablesTree(this);
      scrolled_window.add(variables_tree);

      // Separator
      var separator = new Gtk.Separator(Gtk.Orientation.VERTICAL);
      this.pack_start(separator, false, false, 0);

      // Right side
      variables_editor = new Ros.VariablesEditor(this);
      this.pack_start(variables_editor, false, false, 0);

      // Help
      help.register_step("variables_tree", new Ros.HelpStep(scrolled_window, "This list contains all the Xresouces defintions that you can customize. The list can be sorted by clicking on each colunm header."));
      help.register_step("filter_entry", new Ros.HelpStep(filter_entry, "You can use this search box to filter and search the list."));

      help.order_sequence({"variables_tree", "filter_entry", "variables_editor", "variable_name", "variable_default", "reset_button", "variable_value", "macro_button", "save_button", "is_user_defined", "macro_value", "cancel_button", "write_button"});
    } else {
      var b = Ros.RegolithVersionChecker.get_wrong_version_widget(min_version);
      b.valign = Gtk.Align.CENTER;
      pack_start(b, true, true, 0);
    }
  }

  private void parse_config() {
    macros = new HashMap<string, string>();
    variables = new HashMap<string, Ros.VariableValue>();
    parser_manager = new ParserManager(macros, variables);

    parser_manager.parse();
  }

  public string get_i3bar_icon_font() {
    Regex r = new Regex(".*,\\s*(.*)\\s+[0-9]+");
    var   font = variables["i3-wm.bar.font"].is_macro ? macros[variables["i3-wm.bar.font"].value] : variables["i3-wm.bar.font"].value;

    return r.replace(font, font.length, 0, "\\1" );
  }

  public Gtk.CssProvider get_i3bar_icon_font_css_provider() {
    var css_provider = new Gtk.CssProvider();
    css_provider.load_from_data(".icon_font { font-family: " + get_i3bar_icon_font() + "; }");

    return css_provider;
  }

  // This function is used by the i3b feature
  public void parse_vars_from_new_i3blocks() {
    HashMap<string, Ros.VariableValue> vars = new HashMap<string, Ros.VariableValue>();

    // Get new vars from newly installed blocks
    I3xrocksParser.parse(Ros.ParserManager.i3xrocks_dir(), macros, variables, vars);

    // Add new vars to list tree
    variables_tree.populate(vars);

    // add new vars to variables
    foreach (var v in vars.entries) {
      variables.set(v.key, v.value);
    }
  }

  public Ros.HelpSequence get_help_sequence() {
    if (help != null) {
      // Select first row if nothing is selected
      var selection = variables_tree.get_selection();
      if (selection.count_selected_rows() == 0) {
        var tp = new Gtk.TreePath.from_string("0");
        selection.select_path(tp);
      }
    }

    return help;
  }
}
