using Gee;

public class Ros.VariablesTree : Gtk.TreeView {
  public Gtk.ListStore        store;
  public Gtk.TreeModelSort    sort_model;
  public Gtk.TreeModelFilter  filter_model;
  public Gtk.CellRendererText crt_var_value;
  public Ros.DesktopConfigBox desktop_config;

  public VariablesTree(Ros.DesktopConfigBox desktop_config) {
    store = new Gtk.ListStore(4, typeof (string), typeof (string), typeof (string), typeof (string));
    //this.set_model(store);
    this.desktop_config = desktop_config;

    crt_var_value = new Gtk.CellRendererText ();
    update_i3bar_icon_font();

    this.insert_column_with_attributes (-1, "Name", new Gtk.CellRendererText (), "text", 0);
    this.insert_column_with_attributes (-1, "Value", crt_var_value, "text", 1);
    this.insert_column_with_attributes (-1, "Macro", new Gtk.CellRendererText (), "text", 2);
    this.insert_column_with_attributes (-1, "Custom", new Gtk.CellRendererText (), "text", 3);

    // Make all columns sortable
    var c = this.get_column(0);
    c.set_sort_column_id(0);
    c.set_sort_order(Gtk.SortType.ASCENDING);
    c = this.get_column(1);
    c.set_sort_column_id(1);
    c = this.get_column(2);
    c.set_sort_column_id(2);
    c = this.get_column(3);
    c.set_sort_column_id(3);

    // Populate tree
    populate(desktop_config.variables);

    // Filtering
    filter_model = new Gtk.TreeModelFilter (store, null);
    filter_model.set_visible_func(filter_tree);
    sort_model = new Gtk.TreeModelSort.with_model(filter_model);
    this.set_model(sort_model);

    // Row selection
    var selection = this.get_selection();
    selection.set_mode(Gtk.SelectionMode.SINGLE);

    selection.changed.connect(() => {
      Gtk.TreeModel data;
		  Gtk.TreeIter  it;
      string        variable_key;

      if (selection.get_selected(out data, out it)) {
        data.get(it, 0, out variable_key);
        var vv = desktop_config.variables[variable_key];

        desktop_config.variables_editor.variable_name.label = variable_key;
        desktop_config.variables_editor.default_value.label = vv.default_value;
        desktop_config.variables_editor.variable_value.text = vv.value;

        if (vv.is_macro) {
          desktop_config.variables_editor.is_macro.label = "Yes";
          desktop_config.variables_editor.macro_value.set_markup(desktop_config.macros[vv.value]);
        } else {
          desktop_config.variables_editor.is_macro.label = "No";
          desktop_config.variables_editor.macro_value.label = "";
        }
        desktop_config.variables_editor.is_user_defined.label = vv.is_user_defined ? "Yes" : "No";
        desktop_config.variables_editor.toggle_editor("ON");
      } else {
        desktop_config.variables_editor.toggle_editor("OFF");
      }
    });
  }

  public void populate (HashMap<string, Ros.VariableValue> variables) {
    Gtk.TreeIter iter;

    foreach (var variable in variables.entries) {
      var m = variable.value.is_macro ? "✓" : "";
      var u = variable.value.is_user_defined ? "✓" : "";
      store.append (out iter);
			store.set (iter, 0, variable.key, 1, variable.value.value, 2, m, 3, u);
    }
  }

  public void update_i3bar_icon_font() {
    crt_var_value.font = desktop_config.get_i3bar_icon_font();
  }

  private bool filter_tree(Gtk.TreeModel model, Gtk.TreeIter iter) {
    GLib.Value vk;
    string variable_key;

    model.get_value(iter, 0, out vk);
    variable_key = vk.get_string();

    if (desktop_config.filter_entry.text == "")
      return true;

    if (variable_key.index_of(desktop_config.filter_entry.text) > -1)
      return true;
    else
      return false;
  }
}
