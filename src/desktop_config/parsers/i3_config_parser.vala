using Gee;

public class Ros.I3ConfigParser {
  public static void parse(string[] filenames, HashMap<string, string> *macros, HashMap<string, Ros.VariableValue> *variables) {
    string  line;
    File    file_to_parse = null;

    stdout.printf("\nParsing i3 config file...\n");
    foreach (var filename in filenames) {
      var file = File.new_for_path(filename);
      if (!file.query_exists()) {
        stdout.printf ("  File '%s' doesn't exist.\n", file.get_path());
      } else {
        file_to_parse = file;
        break;
      }
    }

    if (file_to_parse == null) {
        stderr.printf("  No i3 config file found!\n"); // this should generate an exception instead
    } else {
      stdout.printf("  Using '%s'\n", file_to_parse.get_path());
      var stream = new DataInputStream (file_to_parse.read ());
      while ((line = stream.read_line (null)) != null) {
          line = line.strip();
          if (is_variable(line)) {
            var v = parse_variable(line);
            if (!variables->has_key(v[0])) {
              //stdout.printf("[%s]\nVAR: [%s] - [%s]\n\n", line, v[0], v[1]);
              var vv = new Ros.VariableValue();
              vv.value = v[1];
              vv.default_value = v[1];
              variables->set(v[0], vv);
            }
          }
      }
    }
  }

  private static bool is_variable(string line) {
    return line.has_prefix("set_from_resource ");
  }

  private static string[] parse_variable(string line) {
    //set_from_resource $focused.color.border i3-wm.client.focused.color.border "#002b36"
    Regex     regex = new Regex ("(set_from_resource)\\s+([^\\s]*)\\s+([^\\s]*)(\\s+.*)?");
    string[]  sp = regex.split(line);
    string[]  v = { sp[3], sp[4].strip() };
    //stdout.printf("[%s]\n[%s] - [%s] - [%s] - [%s] - [%s]\n\n", line, sp[0], sp[1], sp[2], sp[3], sp[4].strip());

    return v;
  }
}
