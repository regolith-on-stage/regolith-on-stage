public class Ros.I3Blocks : Gtk.Box, Ros.Helpable {
  public Ros.BlockList          block_list;
  public Ros.BarPreview         bar_preview;
  public Ros.ApplicationWindow  parent_window;
  public Ros.HelpSequence       help = null;
  private string                min_version = "1.4";

  public I3Blocks(Ros.ApplicationWindow parent_window) {
    this.parent_window = parent_window;
    orientation = Gtk.Orientation.VERTICAL;
    spacing = 5;

    if (Ros.RegolithVersionChecker.check_compatibility(min_version)) {
      help = new Ros.HelpSequence();

      bar_preview = new Ros.BarPreview(this);
      block_list = new Ros.BlockList(this);
      bar_preview.init(block_list.block_data);

      pack_start(block_list, true, true, 0);
      pack_start(bar_preview, false, false, 0);

      var apply_button = new Gtk.Button.with_label("Apply Changes");
      apply_button.halign = Gtk.Align.END;
      Ros.StyleHelper.add_class(apply_button, "suggested-action");
      pack_start(apply_button, false, false, 0);

      apply_button.clicked.connect(() => {
        var d = new Ros.I3BlockUpdateDialog(parent_window, block_list);
        d.present();
      });

      help.register_step("apply", new Ros.HelpStep(apply_button, "Once you are done, click this button to review your changes before they are applied."));
      help.order_sequence({"block_list", "row", "info", "switch", "preview", "apply"});
    } else {
      var b = Ros.RegolithVersionChecker.get_wrong_version_widget(min_version);
      b.valign = Gtk.Align.CENTER;
      pack_start(b, true, true, 0);
    }
  }

  public Ros.HelpSequence get_help_sequence() {
    return help;
  }
}
