public class Ros.HeaderBar : Gtk.HeaderBar {
  public Ros.ApplicationWindow window;

    public HeaderBar(Ros.ApplicationWindow window) {
      this.window = window;
      title = "Regolith On Stage";
      subtitle = "Ricing Without Staging";
      show_close_button = true;
      set_decoration_layout("menu:close");

      var menu_button = new Gtk.Button.from_icon_name("open-menu-symbolic", Gtk.IconSize.BUTTON);
      pack_end(menu_button);

      var popover = new Gtk.Popover(menu_button);
      var box = new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
      box.margin = 12;
      popover.add(box);
      popover.set_position(Gtk.PositionType.BOTTOM);

      var help_button = new Gtk.ModelButton();
      help_button.text = "Help";
      box.pack_start(help_button, false, true, 0);

      var about_button = new Gtk.ModelButton();
      about_button.text = "About Regolith On Stage";
      box.pack_start(about_button, false, true, 0);

      menu_button.clicked.connect(() => {
        popover.set_relative_to(menu_button);
        popover.show_all();
        popover.popup();
      });

      help_button.clicked.connect(() => {
        var child = window.stack.visible_child as Ros.Helpable;
        var help_sequence = child.get_help_sequence();
        if (help_sequence != null) {
          new Ros.HelpPopover(help_sequence.get_sequence());
        }
      });

      about_button.clicked.connect(() => {
        var dialog = new Ros.AboutDialog(window);
        dialog.present();
      });
    }
}

public class Ros.AboutDialog : Gtk.AboutDialog {
  public AboutDialog(Ros.ApplicationWindow window) {
    this.icon = new Gdk.Pixbuf.from_resource("/logo/regolith-on-stage.svg");
    set_destroy_with_parent(true);
    set_transient_for(window);
    set_modal(true);

    logo =  new Gdk.Pixbuf.from_resource("/logo/regolith-on-stage.svg");

    authors = { "Guillaume Deflaux <incoming+gdeflaux-regolith-on-stage-17741311-74j084f1txih86itzicbj1td0-issue@incoming.gitlab.com>" };
    artists = { "Smalllike from Noun Project", "Alfredo @ IconsAlfredo.com from the Noun Project" };

    program_name = "Regolith On Stage";
    comments = "Ricing without Staging";
    copyright = "Copyright © 2020 Guillaume Deflaux";
    version = "v0.5.2";

    website = "https://regolith-on-stage.gitlab.io";
    website_label = "Website";

    license_type = Gtk.License.GPL_3_0;
  }
}
