<p align="center">
  <img width="100" height="100" src="data/images/logo/regolith-on-stage.svg">
</p>

# Regolith On Stage

Regolith On Stage is a GTK application that allows you to customize your [Regolith](https://regolith-linux.org/) desktop environment without having to edit configuration files manually.

> Ricing without Staging!

Visit the [website](https://regolith-on-stage.gitlab.io) to learn more about the features of the application.

## In a Hurry?

Regolith On Stage is available for **Regolith v1.3.1+** and can be installed on Ubuntu 18.04, 19.10 or 20.04 by adding a PPA and installing the `regolith-on-stage` package. Adding the PPA will allow you to get the latest updates automatically.

```
$ sudo add-apt-repository ppa:gdeflaux/regolith-on-stage
$ sudo apt install regolith-on-stage
```

You can download the binaries and source code of each individual version from the [detailed release notes](https://regolith-on-stage.org/releases).

Visit the [wiki](https://gitlab.com/regolith-on-stage/regolith-on-stage/-/wikis/home) for more information about how the application works and technical documentation.

### Build It Yourself

The application is written in [Vala](https://wiki.gnome.org/Projects/Vala) and is built with [Meson](https://mesonbuild.com/).

```
$ git clone https://gitlab.com/regolith-on-stage/regolith-on-stage.git
$ cd regolith-on-stage
$ meson build
$ cd build
$ ninja
$ ./regolith-on-stage
```

If you want to install the application run `meson install` from the `build` directory.

### Preview

![screenshot](screenshot.png)
